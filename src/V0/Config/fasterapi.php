<?php

return [
    'controller' => app_path('Http/Controllers/Fasterapi'),
    'repository' => app_path( 'Repositories/Fasterapi' ),
    'routes' => base_path( 'routes' ),

    'controller_namespace' => 'App\\Fasterapi\\',
    'repository_namespace' => 'App\\Repositories\\Fasterapi\\',
];