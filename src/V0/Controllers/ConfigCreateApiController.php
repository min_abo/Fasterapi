<?php

namespace Abo\Fasterapi\V0\Controllers;

use Abo\Fasterapi\V0\Logics\ConfigCreateApiLogic;
use Abo\Fasterapi\V0\Logics\RedisCacheLogic;
use Abo\Generalutil\V1\Dto\RequestDto;
use Abo\Generalutil\V1\Exceptions\ApiException;
use Abo\Generalutil\V1\Exceptions\PageException;
use Abo\Generalutil\V1\Utils\ValidateUtil;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ConfigCreateApiController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const REDIS_INFO_CACHE_PREFIX = 'CCA_info_'; // 信息类缓存
    protected $dto, $model, $commonRules = [];

    public function __construct( Model $model )
    {
        $this->model = $model;
        $this->dto = new RequestDto();
    }

    /** @throws ApiException|PageException */
    public function list( Builder $listBuiler )
    {
        $ret2Search = ( new ConfigCreateApiLogic( $this->model ) )->list( $listBuiler );
        $ret2Search['data'] = $this->handleIndex2Show( $ret2Search['data'] );
        return $ret2Search;
    }

    /** @throws ApiException|PageException */
    public function detail( Builder $listBuiler  )
    {
        $ret2Search = ( new ConfigCreateApiLogic( $this->model ) )->detail( $listBuiler );
        $ret2Search = $this->handleRead2Show( $ret2Search );
        return $ret2Search;
    }

    /** 依赖翻转冗余方法 */
    protected function cacheData( string $functionName, Builder $listBuiler )
    {
        $redis = RedisCacheLogic::redisFactory();
        $redisKey = self::REDIS_INFO_CACHE_PREFIX.serialize( json_encode( $listBuiler ) );
        $data2Cache = [];

        if ( $data = $redis->get( $redisKey ) ) {
            if ( !$data ) { throw new ApiException( 605, '暂无更多信息' ); }
            return json_decode( $data, true );
        }
        if ( method_exists( $this, $functionName ) ) {
            $data2Cache = $this->$functionName( $listBuiler );
        }


        if ( $data2Cache ) {
            $redis->set( $redisKey, json_encode( $data2Cache ), 3600 );
        }
        return $data2Cache;
    }

    protected function handleIndex2Show( array $data )
    {
        return $data;
    }

    protected function handleRead2Show( array $data )
    {
        return $data;
    }

    /**
     * 验证参数合法性
     * @param array $form_rules 参数验证规则 | string $class_name | string $function_name
     * @return bool
     * @throws ApiException
     */
    protected function validator( $rules, $msg = [], $className, $functionName )
    {
        $formRules = array_merge( $rules, $this->commonRules );

        $VaildParam = new ValidateUtil();
        $validator = $VaildParam->validator( $formRules, $msg, $className, $functionName );
        if( $validator ){ return true; }

        $logInfo = [
            'url_params' => $this->dto->request(),
            'error' => $validator,
            'function' => $functionName,
            'class' => $className,
        ];

        logger( var_export( $logInfo, true ), 'error' );
        throw new ApiException( 901 );
    }
}
