<?php
/**
 * Function:
 * Description:
 * Abo 2019/2/24 17:49
 * Email: abo2013@foxmail.com
 */

namespace Abo\Fasterapi\V0\Controllers\Spike;

use Abo\Fasterapi\V0\Interfaces\ConfigCreateApiControllerInterface;
use Abo\Fasterapi\V0\Controllers\ConfigCreateApiController;

use App\Model\MobileWeb\Game;

class SpikeActivityController extends ConfigCreateApiController implements ConfigCreateApiControllerInterface
{
    public function __construct()
    {
        parent::__construct( new Game() );
    }

    /** 列表 @throws ApiException */
    public function index()
    {
        $column2Get = [
            'id as game_id', 'game_front_name as game_name', 'icon_link as game_icon', 'game_type',
            'flag_msg', 'game_intro', 'game_website',
        ];

        $listBuiler = Game::select( $column2Get )->where( 'game_status', '=', Game::VAILD_GAME );
        if ( $gameType = $this->dto->request( 'game_type' ) ) { $listBuiler = $listBuiler->where( 'game_type', '=', $gameType ); }
        if ( $isHit = $this->dto->request( 'is_hit' ) ) { $listBuiler = $listBuiler->where( 'flag_msg', '=', 'hit' ); }

        $ret2GiftList = $this->cacheData( 'index', $listBuiler );
        return Response::success()->nums( $ret2GiftList['nums'] )->with( ['gameType' => [], 'game' => $ret2GiftList['data']] )->json();
    }

    /** 详情 @throws ApiException | \App\Exceptions\ErrorPageException */
    public function read()
    {
        $column2Get = [
            'id as game_id', 'game_front_name as game_name', 'icon_link as game_icon', 'game_type',
            'flag_msg', 'game_intro', 'game_website',
        ];

        if ( !$gameId = $this->dto->request( 'game_id' ) ) { throw new ApiException( 500, '更多信息' ); }
        $listBuiler = Game::select( $column2Get )->where( 'id', '=', intval( $gameId ) );

        $ret2GiftList = $this->cacheData( 'read', $listBuiler );
        return Response::success()->with( $ret2GiftList )->json();
    }

    /** 处理列表显示数据 */
    public function handleIndex2Show( array $listData = [] )
    {
        if ( !$listData ) { return $listData; }

        return $listData;
    }

    /** 处理详情显示数据 */
    public function handleRead2Show(array $infoData = [])
    {
        if ( !$infoData ) { return $infoData; }

        return $infoData;
    }
}