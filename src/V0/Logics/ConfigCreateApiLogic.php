<?php
/**
 * Function:
 * Description:
 * Abo 2019/2/2 06:58
 * Email: abo2013@foxmail.com
 */

namespace Abo\Fasterapi\V0\Logics;

use Abo\Fasterapi\V0\Repositories\ConfigCreateApiRepository;
use Abo\Generalutil\V1\Exceptions\ApiException;
use Abo\Generalutil\V1\Exceptions\PageException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ConfigCreateApiLogic
{
    protected $model, $repository;

    /** @throws PageException */
    public function __construct( Model $model )
    {
        if ( !$model ) {
            logger( date( 'md H:i:s' ).'ConfigCreateApiLogic实例化缺失有效Model. 给予值为:'
                .var_export( var_dump( $model ), true ) )
            ;
            throw new PageException( 500, '系统异常,请返回' );
        }

        $this->model = $model;
    }

    /** 列表 @throws ApiException */
    public function list( Builder $Builer )
    {
        return $this->repositoryInstance( $this->model )->list( $Builer );
    }

    /** 详情 @throws ApiException */
    public function detail( Builder $Builer )
    {
        return $this->repositoryInstance( $this->model )->detail( $Builer );
    }

    /** 配置生成Api模型操作层 单例 @return ConfigCreateApiRepository */
    public function repositoryInstance( Model $model )
    {
        if ( $this->repository ) {
            return $this->repository;
        }

        return $this->repository = new ConfigCreateApiRepository( $model );
    }
}