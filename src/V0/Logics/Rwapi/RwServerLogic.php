<?php
/**
 * Function:
 * Description:
 * Abo 2019/1/14 15:12
 * Email: abo2013@foxmail.com
 */

namespace Abo\Fasterapi\V0\Logics\Rwapi;

use Abo\Fasterapi\V0\Repositories\RwRepository;

class RwServerLogic
{
    public $repository;

    public function __construct( string $tableName )
    {
        $this->repository = new RwRepository( $tableName );
    }

    public function add( $data )
    {
        return $this->repository->add( $data );
    }

    public function save( int $id, array $data )
    {
        return $this->repository->save( $id, $data );
    }

    public function del( int $id, string $column = 'id' )
    {
        return $this->repository->del( $id, $column );
    }

    public function getList( $data, $where, $page, $pageSize = 100, $orderBy = '', $groupBy = '' )
    {
        $list = $this->repository->get( $data, $where, $page, $pageSize, $orderBy, $groupBy );
        if ( false === $list[0] ) {
            throw new ApiException( 605, '暂无更多信息' );
        }

        $list[0] = $list[0]->toArray();

        return $list;
    }

    public function sqlQuery( string $sql = '' )
    {
        return $this->repository->query( $sql );
    }
}