<?php
/**
 * Function:
 * Description:
 * Abo 2019/1/21 22:33
 * Email: abo2013@foxmail.com
 */

namespace Abo\Fasterapi\V0\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Abo\Generalutil\V1\Dto\RequestDto;
use Abo\Generalutil\V1\Exceptions\ApiException;
use Abo\Generalutil\V1\Repositories\BaseRepository;

class ConfigCreateApiRepository extends BaseRepository
{
    protected $dto;

    public function __construct(Model $Model)
    {
        parent::__construct( $Model );
        $this->dto = new RequestDto();
    }

    /** 列表 @throws ApiException */
    public function list( Builder $Builder )
    {
        $dataNum = $this->countSearchTotal( $this->Model ); // 总数
        $ret2Search = $Builder
            ->forPage( $this->dto->getPageNow(), $this->dto->getPageSize( env( 'PAGE_NUM', 10 ) ) )
            ->get();
        if ( !$ret2Search ) { throw new ApiException( 605, '暂无更多信息' ); }

        return [ 'data' => $ret2Search->toArray(), 'nums' => $dataNum ];
    }

    /** 详情 @throws ApiException */
    public function detail( Builder $Builder )
    {
        $ret2Search = $Builder->first();
        if ( !$ret2Search ) { throw new ApiException( 605, '暂无更多信息' ); }
        return $ret2Search->toArray();
    }
}