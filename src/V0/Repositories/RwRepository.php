<?php
/**
 * Function:
 * Description:
 * Abo 2019/1/21 20:54
 * Email: abo2013@foxmail.com
 */

namespace Abo\Fasterapi\V0\Repositories;


use Illuminate\Support\Facades\DB;
use Abo\Generalutil\V1\Repositories\BaseRepository;

class RwRepository extends BaseRepository
{
    public function __construct( string $tableName )
    {
        parent::__construct( new $tableName() );
    }

    public function query( string $sql = '' )
    {
        if ( !$sql ) { return []; }
        $num = $this->countSearchTotal( $this->Model ,'', strstr( strval( $sql ), 'ORDER BY', true ) );
        $ret = DB::connection( $this->Model->getConnectionName() )->select( strval( $sql ) );
        $ret = json_decode( json_encode( $ret ), true );
        return [ 'num' => $num, 'data' =>$ret ];
    }

    /** 添加数据 */
    public function add( array $data )
    {
        if ( !$data ) { return true; }
        $insertData = array_filter( $data );

        return $this->Model->insertGetId( $insertData );
    }

    /** 修改数据 */
    public function save( int $id, array $data )
    {
        if ( !$data ) { return true; }
        $updateData = $data;//array_filter( $data );

        return $this->Model->where( 'id', '=', $id )->update( $updateData );
    }

    /** 查询数据 */
    public function get( $column, $where, $page, $pageSize = 100, $order = '', $groupBy = '' )
    {
        $selectColumn = [ '*' ];
        if ( $column ) {
            $selectColumn = array_filter( $column );
        }

        $SearchModel = $this->Model->select( $selectColumn );
        $SearchModel = $this->whereParamHandle( $SearchModel, $where );

        $noms = $this->countSearchTotal( $SearchModel );
        $SearchModel = $SearchModel->forPage( $page, $pageSize );

        $order && $SearchModel->orderBy( $order, 'DESC' );
        $groupBy && $SearchModel->groupBy( $groupBy );
        $data = $SearchModel->get();

        return [ $data, $noms ];
    }

    public function del( int $id, string $column = 'id' )
    {
        return $this->Model->where( $column, '=', $id )->delete();
    }

    /** 搜索条件 */
    protected function whereParamHandle( $Model, array $where )
    {
        if ( array_key_exists( 'and', $where ) ) {
            $Model = $this->andWhereParamHandle( $Model, $where[ 'and' ] );
        }

        if ( array_key_exists( 'like', $where ) ) {
            $Model = $this->likeWhereParamHandle( $Model, $where[ 'like' ] );
        }

        return $Model;
    }

    private function likeWhereParamHandle( $Model, array $where )
    {
        if ( !is_array( $where ) || empty( $where ) ) { return $Model; }

        foreach ( $where as $k2Where => $v2Where ) {
            $Model->where( $k2Where, 'like', "%{$v2Where}%" );
        }

        return $Model;
    }

    private function andWhereParamHandle( $Model, array $where )
    {
        if ( !is_array( $where ) || empty( $where ) ) { return $Model; }

        foreach ( $where as $k2Where => $v2Where ) {
            $Model->where( $k2Where, '=', $v2Where );
        }

        return $Model;
    }
}