<?php
/**
 * Description: 基础数据模型(Model)操作类
 * Abo 2018/1/2 18:05
 * Email: abo2013@foxmail.com
 *
 * @method getInfoByKey( array $where, array $column2Select )                           根据条件搜索单个信息
 * @method getListByKey( $keyName, array $keyId, array $column2Select, array $where )   根据字段,获取列表信息
 * @method countSearchTotal( $Model, $table = '' )                                      获取 符合条件数据 总量
 * @method duplicateKeyInsert( $fileCategoryRelationArr, $tableName = '' )              单个插入更新
 * @method getSqlWithBind( $Model )         获取带参数sql
 */

namespace Abo\Fasterapi\V0\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BaseRepository
{
    protected $Model;

    public function __construct( Model $Model )
    {
        $this->Model = $Model;
    }

    /** 根据条件搜索单个信息  @where [ 'whereName'=>'whereValue' ] */
    public function getInfoByKey( array $where, array $column2Select = [ '*' ] )
    {
        $where = array_filter( $where );
        if ( !$where ) { return false; }

        $infoModel = $this->Model->select( $column2Select );
        if ( $where ) {
            foreach ( $where as $k2Where => $v2Where ) {
                $infoModel->where( $k2Where, '=', $v2Where );
            }
        }

        $info = $infoModel->first();
        if ( false === $info ) { return false; }

        // 获取单一字段时,直接返回
        if ( 1 == count( $column2Select ) && '*' != $column2Select[0] ) {
            return $info->getAttributeValue( $column2Select[0] );
        }

        return $info;
    }

    /** 根据字段,获取列表信息 @where [ 'whereName'=>'whereValue' ] */
    public function getListByKey( $keyName, array $keyId, array $column2Select = [ '*' ], array $where = [] )
    {
        $listModel = $this->Model->select( $column2Select );
        $keyName && $listModel->whereIn( $keyName, $keyId );

        if ( $where ) {
            foreach ( $where as $k2Where => $v2Where ) {
                $listModel = $listModel->where( $k2Where, $v2Where );
            }
        }

        $list = $listModel->get();
        if ( false === $list ) { return false; }

        return $list->toArray();
    }

    /** 获取 符合条件数据 总量 */
    protected function countSearchTotal( $Model, $table = '' )
    {
        $countSql = 'SELECT COUNT(1) as num FROM '.$table;
        if ( !$table ){
            $searchSql = $this->getSqlWithBind( $Model );
            $countSql = 'SELECT COUNT(1) as num FROM ('.$searchSql.') AS t';
            // logger( 'count SQL:' . $countSql );
        }
        $count = DB::connection( $this->Model->getConnectionName() )->select( $countSql );

        $ret = json_decode( json_encode( $count[0] ), true );
        return !array_key_exists( 'num', $ret ) ? 0:$ret['num'];
    }

    /** 单个插入更新 @throws \Exception */
    protected function duplicateKeyInsert( $fileCategoryRelationArr, $tableName = '' )
    {
        $insertColumns = null;
        $updateColumnsKeyArr = $updateColumnsKeyStr = false;
        $tableName = $tableName ?: $this->Model->getTable();

        // 添加部分绑定
        $insertColumns = array_keys( $fileCategoryRelationArr );
        $insertColumnsBindValue = array_values( $fileCategoryRelationArr );

        $insertColumnsQuestionStr = implode( ',', array_fill( 0, count( $fileCategoryRelationArr ), '?' ) );
        $insertColumnsKeyStr = implode( ',', $insertColumns );

        // 更新部分数据绑定
        foreach ( $fileCategoryRelationArr as $k2cate => $v2cate ) {
            $updateColumnsKeyArr[] = "{$k2cate}='{$v2cate}'";
        }

        $updateColumnsKeyStr = implode( ',', $updateColumnsKeyArr );

        $duplicateInsertSql = "insert into {$tableName} ( {$insertColumnsKeyStr} ) "
            . "values ( {$insertColumnsQuestionStr} )  ON DUPLICATE KEY UPDATE {$updateColumnsKeyStr}";
        $ret2Return = DB::connection( $this->Model->getConnectionName() )->insert( $duplicateInsertSql, $insertColumnsBindValue );

        return $ret2Return;
    }

    /** 获取带参数sql */
    private function getSqlWithBind( $Model )
    {
        $bindings = $Model->getBindings();
        $sql = str_replace( '?', '\'%s\'', $Model->toSql() );
        return sprintf( $sql, ...$bindings );
    }
}