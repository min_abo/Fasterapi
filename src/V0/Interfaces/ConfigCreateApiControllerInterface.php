<?php
/**
 * Function: 针对 实体(信息提供) 的接口标准
 * Description:标准化实体接口方法, 为了方便冗余开发
 * Abo 2019/1/20 17:43
 * Email: abo2013@foxmail.com
 */

namespace Abo\Fasterapi\V0\Interfaces;

use Illuminate\Database\Eloquent\Builder;

interface ConfigCreateApiControllerInterface
{
    public function index();

    public function read();
}