<?php

namespace Abo\Fasterapi;

use Abo\Generalutil\V1\Utils\FileUtil;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\ServiceProvider;

class FasterapiServiceProvider extends ServiceProvider
{
    const PACKAGIST_VERSION = 'V0';

    protected $commands = [
        'Abo\Fasterapi\Console\InstallCommand',
        'Abo\Fasterapi\Console\MakeCommand',
    ];

    /** Bootstrap the application services @return void */
    public function boot()
    {
        $this->publishes([
            __DIR__ . self::PACKAGIST_VERSION . '/Config/fasterapi.php' => config_path('fasterapi.php'), // 配置文件

            __DIR__ . self::PACKAGIST_VERSION . '/Resources/assets/js/' => base_path('public/assets/js/'),
            __DIR__ . self::PACKAGIST_VERSION . '/Resources/assets/css/' => base_path('public/assets/css/'),
            __DIR__ . self::PACKAGIST_VERSION . '/Resources/assets/img/' => base_path('public/assets/img/')
        ]);

        // $this->loadRoutesFrom( base_path('routes/fasterapi.php'), '\Fasterapi' ); // 注册路由
        $this->loadViewsFrom(__DIR__ . self::PACKAGIST_VERSION . '/Resources/views', 'fasterapi');

        if ($this->app->runningInConsole()) {

        }
    }

    /** Register the application services. */
    public function register()
    {
        $this->commands( $this->commands );
    }
}
