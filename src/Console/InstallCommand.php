<?php

namespace Abo\Fasterapi\Console;

use Illuminate\Console\Command;

class InstallCommand extends Command
{
    const CONTROLLER_FILE_TYPE = 'controller';
    const REPOSITORY_FILE_TYPE = 'repository';

    /** The console command name. @var string */
    protected $name = 'fasterapi:install';

    /**
     * php artisan admin:make AssetsController --model App\Models\Assets
     *
     * @var string
     */
    protected $description = 'install fasterapi directory';
    protected $directory = '';

    /** Execute the console command. @return void */
    public function handle()
    {
         $this->initFasterapiControllerDirectory();
         $this->initFasterapiRepositoryDirectory();
        
        $CommandInstallLogic = new CommandInstallLogic();
        $CommandInstallLogic->setRouteServiceProvider(); // 设置路由
    }

    /** 初始化 Fastapi Controller 目录 @return void */
    protected function initFasterapiControllerDirectory()
    {
        $this->directory = config('fasterapi.controller');
        if (is_dir($this->directory)) {
            $this->line("<error>{$this->directory} directory already exists !</error> ");
            return;
        }

        $this->makeDir('/');
        $this->line('<info>Fasterapi Controller directory was created:</info> '.str_replace(base_path(), '', $this->directory));

        $this->createExampleController();
    }

    /** create Fasterapi ExampleFormatter Controller @return void*/
    protected function initFasterapiRepositoryDirectory()
    {
        $this->directory = config('fasterapi.repository');

        if (is_dir($this->directory)) {
            $this->line("<error>{$this->directory} directory already exists !</error> ");

            return;
        }

        $this->makeDir('/');
        $this->line('<info>Fasterapi Repository directory was created:</info> '.str_replace(base_path(), '', $this->directory));

        $this->createExampleRepository();
    }

    /** 初始化 Fastapi Repository 目录 @return void */
    protected function createExampleController()
    {
        $path = $this->directory . DIRECTORY_SEPARATOR . 'ExampleController.php';
        $contents = $this->getStub( self::CONTROLLER_FILE_TYPE );

        $this->laravel['files']->put( $path, $contents );
        $this->line('<info>Fasterapi ExampleFormatter Controller was created:</info> '.str_replace(base_path(), '', $path));
    }

    /** create Fasterapi ExampleFormatter Repository @return void*/
    protected function createExampleRepository()
    {
        $path = $this->directory . DIRECTORY_SEPARATOR . 'ExampleRepository.php';
        $contents = $this->getStub( self::REPOSITORY_FILE_TYPE );

        $this->laravel['files']->put( $path, $contents );
        $this->line('<info>Fasterapi ExampleFormatter Repository was created:</info> '.str_replace(base_path(), '', $path));
    }

    /** Get the stub file for the generator @return string */
    protected function getStub( string $fileType )
    {
        $stubName = '';
        if ( self::CONTROLLER_FILE_TYPE == $fileType ) {
            $stubName = 'blankController.stub';
        }elseif ( self::REPOSITORY_FILE_TYPE == $fileType ) {
            $stubName = 'blankRepository.stub';
        }


        if ( !$stubName ) {
            $this->line( '<info>Did not get the file content of '.$fileType.' blank.stub:</info> '.__DIR__.'/stubs/'.$stubName );
            return false;
        }
        return $this->laravel['files']->get( __DIR__.'/stubs/'.$stubName );
    }

    /** Make new directory @param string $path */
    protected function makeDir($path = '')
    {
        $this->laravel['files']->makeDirectory("{$this->directory}/$path", 0755, true, true);
    }

}
