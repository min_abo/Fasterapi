<?php

namespace Abo\Fasterapi\Console;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeCommand extends GeneratorCommand
{
    /** The name and signature of the console command. @var string */
    protected $signature = 'fasterapi:make {name} {--model=}';

    protected $name = 'fasterapi:make';

    /** The console command description. @var string */
    protected $description = 'create fasterapi controller';

    /** Execute the console command. @return mixed */
    public function handle()
    {
        if (!$this->modelExists()) {
            $this->error('Model does not exists !');

            return false;
        }

        parent::handle(); // 创建文件
        $CommandInstallLogic = new CommandInstallLogic();
        $CommandInstallLogic->addFastapiRoute( trim( $this->argument( 'name' ) ) );
    }

    /**
     * stub 模板文件中替换 类名
     *
     * @param string $stub
     * @param string $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);
        $baseClassName = class_basename($this->option('model'));

        $lcClassName = lcfirst($baseClassName);
        $formCallback = "top-{$lcClassName}-list";
        $tableId = "{$lcClassName}-list";

        return str_replace(
            [
                'DummyModelNamespace',
                'DummyModel',
                'formCallback',
                'tableId',
            ],
            [
                $this->option('model'),
                $baseClassName,
                $formCallback,
                $tableId
            ],
            $stub
        );
    }

    /**
     * 获取类的默认命名空间
     *
     * @param string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        $directory = config('fasterapi.controller');

        $namespace = ucfirst(basename($directory));
        return $rootNamespace."\Http\Controllers\\$namespace";
    }


    /**
     * 检查模型是否已存在
     * @return bool
     */
    protected function modelExists()
    {
        $model = $this->option('model');
        if (empty($model)) {
            return true;
        }

        return class_exists($model);
    }

    /**
     * 获取命令参数
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the controller.'],
        ];
    }

    /**
     * 获取命令配置
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['model', null, InputOption::VALUE_REQUIRED,
                'The eloquent model that should be use as controller data source.', ],
        ];
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {

        $name = Str::replaceFirst($this->rootNamespace(), '', $name);
        return $this->laravel['path'].'/'.str_replace('\\', '/', $name).'.php';
    }

    /** Get the stub file for the generator @return string */
    protected function getStub()
    {
        if ($this->option('model')) {
            return __DIR__.'/stubs/ConfigCreateApiController.stub';
        }

        return __DIR__.'/stubs/blankController.stub';
    }
}
